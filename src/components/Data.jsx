import React, { useState } from 'react'

const Data = ({
	title = "",
	status,
	details = "",
	imageSource,
	article,
	videolink
}) => {

	const [hidden, setHidden] = useState(false);

	return (
		<div className='p-5 rounded-md mb-5 border'>
			<div className='flex items-center'>
				<h1 className='mr-2 font-bold text-2xl'>{title}</h1>
				<span className={`text-xs font-medium text-white ${status ? "bg-green-600" : "bg-red-600"} p-1 px-2 rounded-md h-max`}>{status ? "success" : "Failed"}</span>
			</div>
			<div className='text-xs flex mt-2'>
				<span className='pr-1 mr-1 border-r-2'>12 years ago</span>
				<p><a href={article}>Article |</a></p>
				<p><a href={videolink}> Video Here</a></p>
			</div>
			<div className={`flex mt-4 w-full ${hidden ? "hidden" : ""}`}>
				<div className='w-20 h-16 h- object-scale-down mr-5'>
					<img src={imageSource} className='text-xs' alt={title} />
				</div>
				<div className='w-full text-start'>
					<p>{details}</p>
				</div>
			</div>
			<div className='flex mt-4'>
				<button
					onClick={() => setHidden(prevState => !prevState)}
					className='button-21'>
					{hidden ? "View" : "Hide"}
				</button>
			</div>
		</div>
	)
}

export default Data