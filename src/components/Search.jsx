import React from 'react'

const Search = () => {
	return (
		<div>
		<input className=' p-3 mw-100 mb-5 text-sm border rounded-md focus:outline outline-1 outline-gray-400' type="text" placeholder='Type Here...' />
		<button className='button-21 mt-2 mx-5'>Search</button>
		</div>
	)
}

export default Search