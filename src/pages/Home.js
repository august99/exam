import Search from '../components/Search'
import Spinner from '../components/Spinner';
import Data from '../components/Data';
import React, { useEffect, useState } from 'react';

function Home() {
    const [results, setResults] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
 
    useEffect(() => {
       setIsLoading(true);
       const getLaunchData = async () => {
          let result = await fetch("https://api.spacexdata.com/v3/launches");
          result = await result.json();
          console.log(result);
          setResults(() => {
             setIsLoading(false);
             return result;
          });
       }
 
       getLaunchData();
    }, []);
 
    return <div className="App">
       <div className='m-5 border-gray-200 flex flex-col'>
          <Search />
          <div className=' border-gray-200'>
             {
                results.map(item => (
                   <Data
                      // key={item}
                      details={item.details}
                      status={item.launch_success}
                      title={item.mission_name}
                      imageSource={item.links.mission_patch_small}
                      article = {item.links.article_link}
                      videolink = {item.links.video_link} />
                ))
             }
             {isLoading ? <Spinner /> : ""}
 
          </div>
       </div>
    </div>
 }
 
 export default Home;